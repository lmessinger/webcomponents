var shadow = document.querySelector('#bottleTag').createShadowRoot();
var contentsTemplate = document.querySelector('#bottleMessageTemplate')
var clone = document.importNode(contentsTemplate.content, true);
shadow.appendChild(clone);


function updateClicked(nameTagSelector, textBoxSelector) {
  var text = document.querySelector(textBoxSelector);
  document.querySelector(nameTagSelector).textContent = text.value;
  text.value = '';
  text.focus();
}
